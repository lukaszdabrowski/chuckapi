package com.invicta.chuckapi.service

import com.invicta.chuckapi.dtos.Joke
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class ConsumeChuckAPI {

    fun getRandomJoke(): Joke? {
        val randomJoke = RestTemplate().getForObject("http://api.icndb.com/jokes/random", Joke::class.java)
        println(randomJoke.toString())
        return randomJoke
    }

    fun getJokeById(id : Int) : String? {
        var joke = RestTemplate().getForObject("http://api.icndb.com/jokes/{id}",Joke::class.java, id)
        println(joke)
        return joke?.value?.joke
    }

    fun getJokeJSON(id : Int) : String? {
        val joke = RestTemplate().getForObject("http://api.icndb.com/jokes/random",Joke::class.java, id)
        println(joke)
        return joke?.value?.joke
    }
}




package com.invicta.chuckapi.dtos

data class Joke(
        var type: String,
        var value: Value
)

data class Value(
        var id: Int,
        var joke: String,
        var categories: Array<String>
)
package com.invicta.chuckapi.controller



import com.invicta.chuckapi.service.ConsumeChuckAPI
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class RestController {

    @Autowired
    lateinit var consumeChuckAPI: ConsumeChuckAPI

    @GetMapping("/random")
    fun getRandomJoke() = consumeChuckAPI.getRandomJoke()

    @GetMapping("/{id}")
    fun getJokeById(@PathVariable id: Int) = consumeChuckAPI.getJokeById(id)

}